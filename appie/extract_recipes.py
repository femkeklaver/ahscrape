import urllib2
import time
import json

from py2neo import Graph, Node, Relationship

import re


graph = Graph("http://neo4j:user@localhost:7474/db/data/")
# graph.delete_all()


def parse_item(url):
    web = urllib2.urlopen(url)
    data = web.read()
    # print data
    web.close()
    item = etree.item(data)
    return item


def parse_json(url):
    web = urllib2.urlopen(url)
    data = json.loads(web.read())
    return data


def parse_time(timing):
    hours = re.search("(\d+)\su(?=\w?|.)", timing)
    h = int(hours.group(1)) * 60 if hours else 0
    minutes = re.search("(\d+)\smin(?=\w?|.)", timing)
    m = int(minutes.group(1)) if minutes else 0
    return h + m


def set_attr(node, attr, item, info):
    extracted = item[info]
    if extracted:
        node[attr] = extracted[0]


def set_info(node, json_item, infodict):
    for key, info in infodict.iteritems():
        extracted = None
        if type(info) == list:
            if info[0] in json_item:
                extracted = json_item[info[0]][info[1]]
        elif type(info) == str:
            if info in json_item:
                extracted = json_item[info]
        if extracted:
            node[key] = extracted


def extract_recipe(recipe_id):
    errorfile = open("extracterrors.txt", "w")
    # Recipe features
    url = "https://ms.ah.nl/rest/ah/v1/recipe-util/details?id=%s" % recipe_id
    json = parse_json(url)
    recipe = Node("Recipe", id=recipe_id)
    graph.merge(recipe)
    item = json["items"][0]
    recipe['title'] = item["title"]
    rating = item["rating"]["value"]
    recipe['rating'] = rating if rating else 0
    info = {'course': 'course',
            'kcal': ['energy', 'amount'],
            'protein': ['protein', 'amount'],
            'fat': ['fat', 'amount'],
            'carbohydrate': ['carbohydrate', 'amount'],
            'dishtype': 'dishType',
            'technique': 'cookingTechnique',
            'url': 'canonicalUrl',
            'cuisine': 'cuisine'}
    set_info(recipe, item, info)

    if 'waitTime' in item:
        wt = item['waitTime']
        waitingtime = parse_time(wt)
        recipe['waitingtime'] = waitingtime
    if 'cookTime' in item:
        ct = item['cookTime']
        cookingtime = parse_time(ct)
        recipe['cookingtime'] = cookingtime
    if 'totalTime' in item:
        tt = item['totalTime']
        totaltime = parse_time(tt)
        recipe['totaltime'] = totaltime

    try:
        recipe.push()
    except Exception, e:
        error = "Recipe id: %s raises error:\n%s\n" % (recipe_id, e)
        print error
        errorfile.write(error)

    for ingredient in item['ingredients']:
        # print ingredient
        node = Node("Ingredient",
                    name=ingredient['product']['storeLocationSearchTerm'].lower())
        graph.merge(Relationship(recipe, "HAS_INGREDIENT", node, quantity=ingredient['quantity']))

    for tag in item['classifications']:
        node = Node("Tag", name=tag)
        graph.merge(Relationship(recipe, "HAS_TAG", node))


def recipe_exists(recipe_id):
    cur = graph.run("MATCH (n:Recipe) WHERE n.id = '{id}' RETURN n", id=recipe_id)
    if cur.evaluate():
        return True


def request_all():
    # request 25 recipes at a time
    for i in range(0, 17000, 25):
        url = "https://ms.ah.nl/rest/ah/v1/recipe-util/search?maxResults=25&sortReverted=false&=&offset=%i" % i
        json_data = parse_json(url)

        for item in json_data["items"]:
            recipe_id = item["recipeId"]
            if recipe_exists(recipe_id):
                continue
            extract_recipe(recipe_id)

        print "Processed %i, now waiting 10sec." % i
        time.sleep(10)
    print "Done processing, setting idf for ingredients.."
    graph.run("""MATCH (r:Recipe) WITH COUNT(r) AS n_recipes 
                MATCH (i:Ingredient)<--(r:Recipe) 
                WITH i, LOG10(n_recipes/COUNT(r)) AS idf SET i.idf=idf""")


def main():
    request_all()


if __name__ == "__main__":
    main()
