import urllib2

from flask import Flask, render_template
from py2neo import Graph, Path
import re

app = Flask(__name__)


graph = Graph("http://neo4j:user@localhost:7474/db/data/")


@app.route('/')
def bookmarklet():
    recipes = []
    query = """MATCH (r:Recipe {course: 'hoofdgerecht'}) WHERE r.rating > 3 
    RETURN r.title as name, r.url as url, r.cookingtime as cookingtime, 
    r.waitingtime as waitingtime ORDER BY r.rating DESC LIMIT 10"""
    recipes = graph.data(query)
    # for r in graph.data(query):
    #     print r
    #     recipes.append({'name': r[0], 'url': r[1], 'cookingtime': r[2], 'waitingtime': r[3]})
    # print recipes
    return render_template('index.html', recipes=recipes)


@app.route('/<recipeid>')
def get_recipe(recipeid):
    counted = 3

    match_recipes = """MATCH (n:Recipe {id: %s})-->(i:Ingredient)<--(m:Recipe), (n)-->(j:Ingredient) 
WITH n as n, collect(distinct i.name) as matching, count(distinct i) as counted, collect(distinct j) as collection, m as m
WHERE counted >= %i MATCH (m)-->(j:Ingredient) WHERE NOT(j in collection) 
RETURN m.title as name, m.url as url, m.rating as rating, m.cookingtime as cookingtime,
collection, matching, collect(j.name) as to_buy LIMIT 10""" % (recipeid, counted)

    matched = graph.data(match_recipes)

    return render_template('result.html', recipes=matched)

    # if not result:
    #     to_scrape = "https://ms.ah.nl/rest/ah/v1/recipe-util/details?id=" + \
    #         recipeid
    #     update_db(to_scrape)
    #     get_recipe(recipeid)

    # return result['title']



def parse_time(timing):
    hours = re.search("(\d+)\su(?=\w?|.)", timing)

    h = int(hours.group(1)) * 60 if hours else 0
    minutes = re.search("(\d+)\smin(?=\w?|.)", timing)
    m = int(minutes.group(1)) if minutes else 0
    print timing, h, m
    return h + m


if __name__ == '__main__':
    app.debug = True
    app.run()
