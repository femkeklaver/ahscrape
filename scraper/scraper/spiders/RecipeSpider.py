import scrapy
import re

# from pattern.nl import singularize
from scraper.items import RecipeItem, IngredientItem
# from measures import measures


class RecipeSpider(scrapy.Spider):
    name = "recipe"
    allowed_domains = ["ah.nl/allerhande", "www.ah.nl/allerhande", "www.ah.nl"]
    start_urls = ["http://www.ah.nl/allerhande/recepten-zoeken?No=%i" % i for i in range(0, 10, 10)]

    def parse(self, response):
        for r in response.xpath('//section[@class="item recipe "]/header/h4/a/@href'):
            url = response.urljoin(r.extract())
            yield scrapy.Request(url, callback=self.parse_recipe)

    def parse_recipe(self, response):
        item = RecipeItem()
        # extract teaserpart
        item['url'] = response.url
        title = response.xpath("//title/text()").extract_first()
        item['title'] = title.split(" -")[0]
        for sel in response.xpath("//section[@class='teaser']"):
            results = sel.xpath(".//ul[@class='short']")

            item['course'] = results.xpath(".//li[div[@class='icon icon-course']]/span/text()").extract_first()
            item['kcal'] = results.xpath(".//li[div[@class='icon icon-nutritional']]/span/text()").extract_first().split()[0]
            timing = sel.xpath(
                ".//ul[@class='short']/li[@class='cooking-time']/ul")
            cookingtime = self.parse_time(timing.xpath(
                "li[@content]/text()").extract_first())
            wtime = timing.xpath(
                "li[not(@content)]/text()").extract_first()
            waitingtime = self.parse_time(wtime) if wtime else 0

            item['cookingtime'] = cookingtime
            item['waitingtime'] = waitingtime
            item['totaltime'] = cookingtime + waitingtime

            # item['rating'] = sel.xpath(
            #     ".//span[@class='all-rates']/span/text()")[0].extract()
            yield item

        ingredients = IngredientItem()
        i_list = []
        for sel in response.xpath("//li[@itemprop='ingredients']/a/@data-search-term"):
            i_list.append(sel.extract())
        item['ingredients'] = i_list
        yield item

    def parse_time(self, timing):
        hours = re.search("(\d+)\su(?=\w?|.)", timing)

        h = int(hours.group(1)) * 60 if hours else 0
        minutes = re.search("(\d+)\smin(?=\w?|.)", timing)
        m = int(minutes.group(1)) if minutes else 0
        print timing, h, m
        return h + m


          # filename = response.url.split("/")[-2] + '.html'
          # with open(filename, 'wb') as f:
          #     f.write(response.body)

    # def parse_ingredient(self, ingredient):
    #     parts = ingredient.split()
    #     measure = "|".join(measures.split())
    #     re.search("(?=%s)(\w)", ingredient)
    #     #remove if int in part

    #     #remove if measure in part
    #     sing = singularize(ingredient)
