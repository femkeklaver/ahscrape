# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class RecipeItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    rating = scrapy.Field()
    cookingtime = scrapy.Field()
    waitingtime = scrapy.Field()
    totaltime = scrapy.Field()
    course = scrapy.Field()
    kcal = scrapy.Field()
    ingredients = scrapy.Field(type='list', default=list)
    pass

class IngredientItem(scrapy.Item):
	ingredients = scrapy.Field(type='list', default=list)
