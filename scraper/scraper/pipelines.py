# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from py2neo import Graph, Relationship



class Neo4Pipeline(object):

    def process_item(self, item, spider):
        graph = Graph("http://neo4j:user@localhost:7474/db/data/")
        nodes = []
        # for i in item['ingredients']:
        #     node = graph.merge_one("Ingredient", "name", i)
        #     nodes.append(node)

        recipe = graph.merge_one("Recipe", "title", item['title'])
        for k, v in item.iteritems():
            if k is 'title':
                continue
            elif k is 'ingredients':
                for i in v:
                    node = graph.merge_one("Ingredient", "name", i)
                    graph.create_unique(Relationship(recipe, "HAS_INGREDIENT", node))
            else:
                recipe[k] = v
        recipe.set_labels(item['course'])
        recipe.push()

        # for node in nodes:
        #     graph.create_unique(Relationship(recipe, "HAS_INGREDIENT", node))

        return item

    def clean_ingredient(self, ingredient):
        measures = ("el g tl teen ml takjes ml takje struik struiken eetlepel"
            "eetlepels schaaltje schaaltjes blik bliken pakje pakjes duopak"
            "zak zakje zakjes zakken liter l geraspte gemalen kleine losgeklopt"
            "geklopt")
        parts = ingredient.lower().split()
        cleaned = []
        for p in parts:
            if p not in measures:
                cleaned.append(p)
        return " ".join(cleaned)
