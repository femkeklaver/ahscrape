from py2neo import Graph, Node, Path, Relationship

graph = Graph("http://neo4j:user@localhost:7474/db/data/")
graph.delete_all()

tomaat = graph.merge_one("Ingredient", "naam", "tomaat")
gehakt = graph.merge_one("Ingredient", "naam", "gehakt")
kaas = graph.merge_one("Ingredient", "naam", "kaas")
paprika = graph.merge_one("Ingredient", "naam", "paprika")
komkommer = graph.merge_one("Ingredient", "naam", "komkommer")
spaghetti = graph.merge_one("Ingredient", "naam", "spaghetti")
lasagnebladen = graph.merge_one("Ingredient", "naam", "lasagnebladen")

# tomaat.add_labels("Groente")
gehakt.add_labels("Vlees")
komkommer.add_labels("Groente")

lasagne = graph.merge_one("Recept", "naam", "lasagne")
spaghettires = graph.merge_one("Recept", "naam", "spaghetti")
komkommersla = graph.merge_one("Recept", "naam", "komkommersla")

graph.create_unique(Relationship(lasagne, "HAS_INGREDIENT", tomaat))
graph.create_unique(Relationship(lasagne, "HAS_INGREDIENT", gehakt))
graph.create_unique(Relationship(lasagne, "HAS_INGREDIENT", kaas))
graph.create_unique(Relationship(lasagne, "HAS_INGREDIENT", lasagnebladen))
graph.create_unique(Relationship(lasagne, "HAS_INGREDIENT", komkommer))

graph.create_unique(Relationship(spaghettires, "HAS_INGREDIENT", kaas))
graph.create_unique(Relationship(spaghettires, "HAS_INGREDIENT", tomaat))
graph.create_unique(Relationship(spaghettires, "HAS_INGREDIENT", gehakt))
graph.create_unique(Relationship(spaghettires, "HAS_INGREDIENT", spaghetti))

graph.create_unique(Relationship(komkommersla, "HAS_INGREDIENT", komkommer))
graph.create_unique(Relationship(komkommersla, "HAS_INGREDIENT", tomaat))
